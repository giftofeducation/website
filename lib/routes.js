const routes = [
  {
    label: "Home",
    path: "/",
  },
  {
    label: "About",
    path: "/about",
  },
  {
    label: "Members",
    path: "/members",
  },
  {
    label: "Projects",
    path: "/projects",
  },
  {
    label: "Blog",
    path: "/blog",
  },
  {
    label: "Donate",
    path: "/donate",
  },
];

export default routes;
