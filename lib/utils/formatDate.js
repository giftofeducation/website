export default function formatDate(dateTimeString) {
  const date = new Date(dateTimeString);

  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };

  // Format Iso String to: Thursday, February 8, 2024
  return date.toLocaleDateString("en-US", options);
}
