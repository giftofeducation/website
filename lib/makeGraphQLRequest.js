const space = process.env.CONTENTFUL_SPACE_ID;
const token = process.env.CONTENTFUL_ACCESS_TOKEN;

export async function makeGraphQLRequest({ query, variables }) {
  try {
    const response = await fetch(
      `https://graphql.contentful.com/content/v1/spaces/${space}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ query, variables }),
      },
    );
    const { data } = await response.json();
    return data;
  } catch (error) {
    console.error("Error making GraphQL request:", error);
    throw error;
  }
}
