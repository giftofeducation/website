export const membersQuery = `
  {
    groupedMembersByRoleCollection(limit: 8) {
      items {
        orgSection
        membersCollection {
          items {
            sys {
              id
            }
            name
            profilePicture {
              url
            }
            memberRole
            memberCountry
            teamRole
            executiveBoardRole
            instructorRole
            assistantRole
            affiliateRole
            about
          }
        }
      }
    }
  }
`;
