export const donationQuery = `
  {
    donationPageContent(id: "77rpJQx0H2eRRcQ9b8NjAy") {
      bannerImage {
        url
      }
      pageTitle
      donationAppeal
      donationMethodsCollection {
        items {
          sys {
            id
          }
          donationImage {
            url
          }
          donationUrl
        }
      }
      donationMethodsDescription
      donationMethodsSectionHeading
    }
  }
`;
