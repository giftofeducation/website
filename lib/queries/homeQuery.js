const homeQuery = `
{
  homePageContent(id: "7zIWyI9aSYkKdupexlnnCc") {
    heroImage {
      url
      description
    }
    heroTagline
    missionStatement
    visionStatement
    bannerHeadingText
    bannerTextField
    qrCodesCollection {
      items {
        sys {
          id
        }
        donationImage {
          url
        }
        donationUrl
      }
    }
  }
}
`;

export default homeQuery;
