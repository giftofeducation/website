const aboutQuery = `
{
  aboutPageContent(id: "1tUWi8ttG19oBcL1AVmLjX") {
    heroHeading
    aboutUs
  }
}
`;

export default aboutQuery;
