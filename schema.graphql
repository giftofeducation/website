"""Exposes a URL that specifies the behavior of this scalar."""
directive @specifiedBy(
  """The URL that specifies the behavior of this scalar."""
  url: String!
) on SCALAR

interface _Node {
  _id: ID!
}

"""
[See type definition](https://app.contentful.com/spaces/9bvr8ot1gwtw/content_types/aboutPageContent)
"""
type AboutPageContent implements Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  linkedFrom(allowedLocales: [String]): AboutPageContentLinkingCollections
  aboutUsContent(locale: String): String
}

type AboutPageContentCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [AboutPageContent]!
}

input AboutPageContentFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  aboutUsContent_exists: Boolean
  aboutUsContent: String
  aboutUsContent_not: String
  aboutUsContent_in: [String]
  aboutUsContent_not_in: [String]
  aboutUsContent_contains: String
  aboutUsContent_not_contains: String
  OR: [AboutPageContentFilter]
  AND: [AboutPageContentFilter]
}

type AboutPageContentLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
}

enum AboutPageContentOrder {
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

"""Represents a binary file in a space. An asset can be any file type."""
type Asset {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  title(locale: String): String
  description(locale: String): String
  contentType(locale: String): String
  fileName(locale: String): String
  size(locale: String): Int
  url(transform: ImageTransformOptions, locale: String): String
  width(locale: String): Int
  height(locale: String): Int
  linkedFrom(allowedLocales: [String]): AssetLinkingCollections
}

type AssetCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [Asset]!
}

input AssetFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  title_exists: Boolean
  title: String
  title_not: String
  title_in: [String]
  title_not_in: [String]
  title_contains: String
  title_not_contains: String
  description_exists: Boolean
  description: String
  description_not: String
  description_in: [String]
  description_not_in: [String]
  description_contains: String
  description_not_contains: String
  url_exists: Boolean
  url: String
  url_not: String
  url_in: [String]
  url_not_in: [String]
  url_contains: String
  url_not_contains: String
  size_exists: Boolean
  size: Int
  size_not: Int
  size_in: [Int]
  size_not_in: [Int]
  size_gt: Int
  size_gte: Int
  size_lt: Int
  size_lte: Int
  contentType_exists: Boolean
  contentType: String
  contentType_not: String
  contentType_in: [String]
  contentType_not_in: [String]
  contentType_contains: String
  contentType_not_contains: String
  fileName_exists: Boolean
  fileName: String
  fileName_not: String
  fileName_in: [String]
  fileName_not_in: [String]
  fileName_contains: String
  fileName_not_contains: String
  width_exists: Boolean
  width: Int
  width_not: Int
  width_in: [Int]
  width_not_in: [Int]
  width_gt: Int
  width_gte: Int
  width_lt: Int
  width_lte: Int
  height_exists: Boolean
  height: Int
  height_not: Int
  height_in: [Int]
  height_not_in: [Int]
  height_gt: Int
  height_gte: Int
  height_lt: Int
  height_lte: Int
  OR: [AssetFilter]
  AND: [AssetFilter]
}

type AssetLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
  blogPostCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): BlogPostCollection
  organizationMemberCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): OrganizationMemberCollection
  facesOfGoeCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): FacesOfGoeCollection
  homePageContentCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): HomePageContentCollection
}

enum AssetOrder {
  url_ASC
  url_DESC
  size_ASC
  size_DESC
  contentType_ASC
  contentType_DESC
  fileName_ASC
  fileName_DESC
  width_ASC
  width_DESC
  height_ASC
  height_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

"""
Create your Posts/Articles here! [See type definition](https://app.contentful.com/spaces/9bvr8ot1gwtw/content_types/blogPost)
"""
type BlogPost implements Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  linkedFrom(allowedLocales: [String]): BlogPostLinkingCollections
  postType(locale: String): String
  isPublicPost(locale: String): Boolean
  postBannerImage(preview: Boolean, locale: String): Asset
  title(locale: String): String
  description(locale: String): String
  author(preview: Boolean, locale: String, where: OrganizationMemberFilter): OrganizationMember
  dateTime(locale: String): DateTime
  tags(locale: String): [String]
  body(locale: String): String
  httpsyaledailynewsComwinterSymposium202420(preview: Boolean, locale: String): Asset
}

type BlogPostCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [BlogPost]!
}

input BlogPostFilter {
  author: cfOrganizationMemberNestedFilter
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  postType_exists: Boolean
  postType: String
  postType_not: String
  postType_in: [String]
  postType_not_in: [String]
  postType_contains: String
  postType_not_contains: String
  isPublicPost_exists: Boolean
  isPublicPost: Boolean
  isPublicPost_not: Boolean
  postBannerImage_exists: Boolean
  title_exists: Boolean
  title: String
  title_not: String
  title_in: [String]
  title_not_in: [String]
  title_contains: String
  title_not_contains: String
  description_exists: Boolean
  description: String
  description_not: String
  description_in: [String]
  description_not_in: [String]
  description_contains: String
  description_not_contains: String
  author_exists: Boolean
  dateTime_exists: Boolean
  dateTime: DateTime
  dateTime_not: DateTime
  dateTime_in: [DateTime]
  dateTime_not_in: [DateTime]
  dateTime_gt: DateTime
  dateTime_gte: DateTime
  dateTime_lt: DateTime
  dateTime_lte: DateTime
  tags_exists: Boolean
  tags_contains_all: [String]
  tags_contains_some: [String]
  tags_contains_none: [String]
  body_exists: Boolean
  body: String
  body_not: String
  body_in: [String]
  body_not_in: [String]
  body_contains: String
  body_not_contains: String
  httpsyaledailynewsComwinterSymposium202420_exists: Boolean
  OR: [BlogPostFilter]
  AND: [BlogPostFilter]
}

type BlogPostLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
  organizationMemberCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, order: [BlogPostLinkingCollectionsOrganizationMemberCollectionOrder]): OrganizationMemberCollection
  updatesAlertCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, order: [BlogPostLinkingCollectionsUpdatesAlertCollectionOrder]): UpdatesAlertCollection
}

enum BlogPostLinkingCollectionsOrganizationMemberCollectionOrder {
  name_ASC
  name_DESC
  memberOfExecutiveBoard_ASC
  memberOfExecutiveBoard_DESC
  executiveBoardRole_ASC
  executiveBoardRole_DESC
  instructorOrIntern_ASC
  instructorOrIntern_DESC
  instructorRole_ASC
  instructorRole_DESC
  teamMember_ASC
  teamMember_DESC
  teamRole_ASC
  teamRole_DESC
  partnerOrSponsor_ASC
  partnerOrSponsor_DESC
  partnersAndSponsorsRole_ASC
  partnersAndSponsorsRole_DESC
  memberLocation_ASC
  memberLocation_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

enum BlogPostLinkingCollectionsUpdatesAlertCollectionOrder {
  title_ASC
  title_DESC
  alertEnabled_ASC
  alertEnabled_DESC
  nonContentRelatedAlert_ASC
  nonContentRelatedAlert_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

enum BlogPostOrder {
  postType_ASC
  postType_DESC
  isPublicPost_ASC
  isPublicPost_DESC
  title_ASC
  title_DESC
  description_ASC
  description_DESC
  dateTime_ASC
  dateTime_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

input cfBlogPostNestedFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  postType_exists: Boolean
  postType: String
  postType_not: String
  postType_in: [String]
  postType_not_in: [String]
  postType_contains: String
  postType_not_contains: String
  isPublicPost_exists: Boolean
  isPublicPost: Boolean
  isPublicPost_not: Boolean
  postBannerImage_exists: Boolean
  title_exists: Boolean
  title: String
  title_not: String
  title_in: [String]
  title_not_in: [String]
  title_contains: String
  title_not_contains: String
  description_exists: Boolean
  description: String
  description_not: String
  description_in: [String]
  description_not_in: [String]
  description_contains: String
  description_not_contains: String
  author_exists: Boolean
  dateTime_exists: Boolean
  dateTime: DateTime
  dateTime_not: DateTime
  dateTime_in: [DateTime]
  dateTime_not_in: [DateTime]
  dateTime_gt: DateTime
  dateTime_gte: DateTime
  dateTime_lt: DateTime
  dateTime_lte: DateTime
  tags_exists: Boolean
  tags_contains_all: [String]
  tags_contains_some: [String]
  tags_contains_none: [String]
  body_exists: Boolean
  body: String
  body_not: String
  body_in: [String]
  body_not_in: [String]
  body_contains: String
  body_not_contains: String
  httpsyaledailynewsComwinterSymposium202420_exists: Boolean
  OR: [cfBlogPostNestedFilter]
  AND: [cfBlogPostNestedFilter]
}

input cfOrganizationMemberNestedFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  name_exists: Boolean
  name: String
  name_not: String
  name_in: [String]
  name_not_in: [String]
  name_contains: String
  name_not_contains: String
  profilePicture_exists: Boolean
  aboutMember_exists: Boolean
  aboutMember_contains: String
  aboutMember_not_contains: String
  postsCollection_exists: Boolean
  orgSection_exists: Boolean
  orgSection_contains_all: [String]
  orgSection_contains_some: [String]
  orgSection_contains_none: [String]
  memberOfExecutiveBoard_exists: Boolean
  memberOfExecutiveBoard: Boolean
  memberOfExecutiveBoard_not: Boolean
  executiveBoardRole_exists: Boolean
  executiveBoardRole: String
  executiveBoardRole_not: String
  executiveBoardRole_in: [String]
  executiveBoardRole_not_in: [String]
  executiveBoardRole_contains: String
  executiveBoardRole_not_contains: String
  instructorOrIntern_exists: Boolean
  instructorOrIntern: Boolean
  instructorOrIntern_not: Boolean
  instructorOrInternRole_exists: Boolean
  instructorOrInternRole_contains_all: [String]
  instructorOrInternRole_contains_some: [String]
  instructorOrInternRole_contains_none: [String]
  instructorRole_exists: Boolean
  instructorRole: String
  instructorRole_not: String
  instructorRole_in: [String]
  instructorRole_not_in: [String]
  instructorRole_contains: String
  instructorRole_not_contains: String
  teamMember_exists: Boolean
  teamMember: Boolean
  teamMember_not: Boolean
  teamRole_exists: Boolean
  teamRole: String
  teamRole_not: String
  teamRole_in: [String]
  teamRole_not_in: [String]
  teamRole_contains: String
  teamRole_not_contains: String
  partnerOrSponsor_exists: Boolean
  partnerOrSponsor: Boolean
  partnerOrSponsor_not: Boolean
  partnersAndSponsorsRole_exists: Boolean
  partnersAndSponsorsRole: String
  partnersAndSponsorsRole_not: String
  partnersAndSponsorsRole_in: [String]
  partnersAndSponsorsRole_not_in: [String]
  partnersAndSponsorsRole_contains: String
  partnersAndSponsorsRole_not_contains: String
  memberLocation_exists: Boolean
  memberLocation: String
  memberLocation_not: String
  memberLocation_in: [String]
  memberLocation_not_in: [String]
  memberLocation_contains: String
  memberLocation_not_contains: String
  memberRole_exists: Boolean
  memberRole_contains_all: [String]
  memberRole_contains_some: [String]
  memberRole_contains_none: [String]
  OR: [cfOrganizationMemberNestedFilter]
  AND: [cfOrganizationMemberNestedFilter]
}

type ContentfulMetadata {
  tags: [ContentfulTag]!
}

input ContentfulMetadataFilter {
  tags_exists: Boolean
  tags: ContentfulMetadataTagsFilter
}

input ContentfulMetadataTagsFilter {
  id_contains_all: [String]
  id_contains_some: [String]
  id_contains_none: [String]
}

"""
Represents a tag entity for finding and organizing content easily.
    Find out more here: https://www.contentful.com/developers/docs/references/content-delivery-api/#/reference/content-tags
"""
type ContentfulTag {
  id: String
  name: String
}

"""
A date-time string at UTC, such as 2007-12-03T10:15:30Z,
    compliant with the 'date-time' format outlined in section 5.6 of
    the RFC 3339 profile of the ISO 8601 standard for representation
    of dates and times using the Gregorian calendar.
"""
scalar DateTime

"""
The 'Dimension' type represents dimensions as whole numeric values between `1` and `4000`.
"""
scalar Dimension

interface Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
}

type EntryCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [Entry]!
}

input EntryFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  OR: [EntryFilter]
  AND: [EntryFilter]
}

enum EntryOrder {
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

"""
Photo Gallery highlighting all the great things GOE does [See type definition](https://app.contentful.com/spaces/9bvr8ot1gwtw/content_types/facesOfGoe)
"""
type FacesOfGoe implements Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  linkedFrom(allowedLocales: [String]): FacesOfGoeLinkingCollections
  galleryImage(preview: Boolean, locale: String): Asset
  descriptionOfImage(locale: String): String
  yearLabel(locale: String): Int
}

type FacesOfGoeCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [FacesOfGoe]!
}

input FacesOfGoeFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  galleryImage_exists: Boolean
  descriptionOfImage_exists: Boolean
  descriptionOfImage: String
  descriptionOfImage_not: String
  descriptionOfImage_in: [String]
  descriptionOfImage_not_in: [String]
  descriptionOfImage_contains: String
  descriptionOfImage_not_contains: String
  yearLabel_exists: Boolean
  yearLabel: Int
  yearLabel_not: Int
  yearLabel_in: [Int]
  yearLabel_not_in: [Int]
  yearLabel_gt: Int
  yearLabel_gte: Int
  yearLabel_lt: Int
  yearLabel_lte: Int
  OR: [FacesOfGoeFilter]
  AND: [FacesOfGoeFilter]
}

type FacesOfGoeLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
}

enum FacesOfGoeOrder {
  descriptionOfImage_ASC
  descriptionOfImage_DESC
  yearLabel_ASC
  yearLabel_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

"""The 'HexColor' type represents color in `rgb:ffffff` string format."""
scalar HexColor

"""
Manage the homepage content here! [See type definition](https://app.contentful.com/spaces/9bvr8ot1gwtw/content_types/homePageContent)
"""
type HomePageContent implements Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  linkedFrom(allowedLocales: [String]): HomePageContentLinkingCollections
  heroImage(preview: Boolean, locale: String): Asset
  heroTagline(locale: String): String
  missionStatement(locale: String): String
  visionStatement(locale: String): String
  donationAppeal(locale: String): String
  donationAppealBackgroundImage(preview: Boolean, locale: String): Asset
  contentfulAttribution(locale: String): JSON
}

type HomePageContentCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [HomePageContent]!
}

input HomePageContentFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  heroImage_exists: Boolean
  heroTagline_exists: Boolean
  heroTagline: String
  heroTagline_not: String
  heroTagline_in: [String]
  heroTagline_not_in: [String]
  heroTagline_contains: String
  heroTagline_not_contains: String
  missionStatement_exists: Boolean
  missionStatement: String
  missionStatement_not: String
  missionStatement_in: [String]
  missionStatement_not_in: [String]
  missionStatement_contains: String
  missionStatement_not_contains: String
  visionStatement_exists: Boolean
  visionStatement: String
  visionStatement_not: String
  visionStatement_in: [String]
  visionStatement_not_in: [String]
  visionStatement_contains: String
  visionStatement_not_contains: String
  donationAppeal_exists: Boolean
  donationAppeal: String
  donationAppeal_not: String
  donationAppeal_in: [String]
  donationAppeal_not_in: [String]
  donationAppeal_contains: String
  donationAppeal_not_contains: String
  donationAppealBackgroundImage_exists: Boolean
  contentfulAttribution_exists: Boolean
  OR: [HomePageContentFilter]
  AND: [HomePageContentFilter]
}

type HomePageContentLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
}

enum HomePageContentOrder {
  heroTagline_ASC
  heroTagline_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

enum ImageFormat {
  """JPG image format."""
  JPG

  """
  Progressive JPG format stores multiple passes of an image in progressively higher detail.
          When a progressive image is loading, the viewer will first see a lower quality pixelated version which
          will gradually improve in detail, until the image is fully downloaded. This is to display an image as
          early as possible to make the layout look as designed.
  """
  JPG_PROGRESSIVE

  """PNG image format"""
  PNG

  """
  8-bit PNG images support up to 256 colors and weigh less than the standard 24-bit PNG equivalent.
          The 8-bit PNG format is mostly used for simple images, such as icons or logos.
  """
  PNG8

  """WebP image format."""
  WEBP
  AVIF
}

enum ImageResizeFocus {
  """Focus the resizing on the center."""
  CENTER

  """Focus the resizing on the top."""
  TOP

  """Focus the resizing on the top right."""
  TOP_RIGHT

  """Focus the resizing on the right."""
  RIGHT

  """Focus the resizing on the bottom right."""
  BOTTOM_RIGHT

  """Focus the resizing on the bottom."""
  BOTTOM

  """Focus the resizing on the bottom left."""
  BOTTOM_LEFT

  """Focus the resizing on the left."""
  LEFT

  """Focus the resizing on the top left."""
  TOP_LEFT

  """Focus the resizing on the largest face."""
  FACE

  """Focus the resizing on the area containing all the faces."""
  FACES
}

enum ImageResizeStrategy {
  """Resizes the image to fit into the specified dimensions."""
  FIT

  """
  Resizes the image to the specified dimensions, padding the image if needed.
          Uses desired background color as padding color.
  """
  PAD

  """
  Resizes the image to the specified dimensions, cropping the image if needed.
  """
  FILL

  """
  Resizes the image to the specified dimensions, changing the original aspect ratio if needed.
  """
  SCALE

  """
  Crops a part of the original image to fit into the specified dimensions.
  """
  CROP

  """Creates a thumbnail from the image."""
  THUMB
}

input ImageTransformOptions {
  """Desired width in pixels. Defaults to the original image width."""
  width: Dimension

  """Desired height in pixels. Defaults to the original image height."""
  height: Dimension

  """
  Desired quality of the image in percents.
          Used for `PNG8`, `JPG`, `JPG_PROGRESSIVE` and `WEBP` formats.
  """
  quality: Quality

  """
  Desired corner radius in pixels.
          Results in an image with rounded corners (pass `-1` for a full circle/ellipse).
          Defaults to `0`. Uses desired background color as padding color,
          unless the format is `JPG` or `JPG_PROGRESSIVE` and resize strategy is `PAD`, then defaults to white.
  """
  cornerRadius: Int

  """Desired resize strategy. Defaults to `FIT`."""
  resizeStrategy: ImageResizeStrategy

  """Desired resize focus area. Defaults to `CENTER`."""
  resizeFocus: ImageResizeFocus

  """
  Desired background color, used with corner radius or `PAD` resize strategy.
          Defaults to transparent (for `PNG`, `PNG8` and `WEBP`) or white (for `JPG` and `JPG_PROGRESSIVE`).
  """
  backgroundColor: HexColor

  """Desired image format. Defaults to the original image format."""
  format: ImageFormat
}

"""
The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf).
"""
scalar JSON

"""
Manage GOE members/Sponsors, their privileges (when applicable), and
relationship to GOE [See type definition](https://app.contentful.com/spaces/9bvr8ot1gwtw/content_types/organizationMember)
"""
type OrganizationMember implements Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  linkedFrom(allowedLocales: [String]): OrganizationMemberLinkingCollections
  name(locale: String): String
  profilePicture(preview: Boolean, locale: String): Asset
  aboutMember(locale: String): OrganizationMemberAboutMember
  postsCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: BlogPostFilter, order: [OrganizationMemberPostsCollectionOrder]): OrganizationMemberPostsCollection
  orgSection(locale: String): [String]
  memberOfExecutiveBoard(locale: String): Boolean
  executiveBoardRole(locale: String): String
  instructorOrIntern(locale: String): Boolean
  instructorOrInternRole(locale: String): [String]
  instructorRole(locale: String): String
  teamMember(locale: String): Boolean
  teamRole(locale: String): String
  partnerOrSponsor(locale: String): Boolean
  partnersAndSponsorsRole(locale: String): String
  memberLocation(locale: String): String
  memberRole(locale: String): [String]
}

type OrganizationMemberAboutMember {
  json: JSON!
  links: OrganizationMemberAboutMemberLinks!
}

type OrganizationMemberAboutMemberAssets {
  hyperlink: [Asset]!
  block: [Asset]!
}

type OrganizationMemberAboutMemberEntries {
  inline: [Entry]!
  hyperlink: [Entry]!
  block: [Entry]!
}

type OrganizationMemberAboutMemberLinks {
  entries: OrganizationMemberAboutMemberEntries!
  assets: OrganizationMemberAboutMemberAssets!
  resources: OrganizationMemberAboutMemberResources!
}

type OrganizationMemberAboutMemberResources {
  block: [OrganizationMemberAboutMemberResourcesBlock!]!
  inline: [OrganizationMemberAboutMemberResourcesInline!]!
  hyperlink: [OrganizationMemberAboutMemberResourcesHyperlink!]!
}

type OrganizationMemberAboutMemberResourcesBlock implements ResourceLink {
  sys: ResourceSys!
}

type OrganizationMemberAboutMemberResourcesHyperlink implements ResourceLink {
  sys: ResourceSys!
}

type OrganizationMemberAboutMemberResourcesInline implements ResourceLink {
  sys: ResourceSys!
}

type OrganizationMemberCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [OrganizationMember]!
}

input OrganizationMemberFilter {
  posts: cfBlogPostNestedFilter
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  name_exists: Boolean
  name: String
  name_not: String
  name_in: [String]
  name_not_in: [String]
  name_contains: String
  name_not_contains: String
  profilePicture_exists: Boolean
  aboutMember_exists: Boolean
  aboutMember_contains: String
  aboutMember_not_contains: String
  postsCollection_exists: Boolean
  orgSection_exists: Boolean
  orgSection_contains_all: [String]
  orgSection_contains_some: [String]
  orgSection_contains_none: [String]
  memberOfExecutiveBoard_exists: Boolean
  memberOfExecutiveBoard: Boolean
  memberOfExecutiveBoard_not: Boolean
  executiveBoardRole_exists: Boolean
  executiveBoardRole: String
  executiveBoardRole_not: String
  executiveBoardRole_in: [String]
  executiveBoardRole_not_in: [String]
  executiveBoardRole_contains: String
  executiveBoardRole_not_contains: String
  instructorOrIntern_exists: Boolean
  instructorOrIntern: Boolean
  instructorOrIntern_not: Boolean
  instructorOrInternRole_exists: Boolean
  instructorOrInternRole_contains_all: [String]
  instructorOrInternRole_contains_some: [String]
  instructorOrInternRole_contains_none: [String]
  instructorRole_exists: Boolean
  instructorRole: String
  instructorRole_not: String
  instructorRole_in: [String]
  instructorRole_not_in: [String]
  instructorRole_contains: String
  instructorRole_not_contains: String
  teamMember_exists: Boolean
  teamMember: Boolean
  teamMember_not: Boolean
  teamRole_exists: Boolean
  teamRole: String
  teamRole_not: String
  teamRole_in: [String]
  teamRole_not_in: [String]
  teamRole_contains: String
  teamRole_not_contains: String
  partnerOrSponsor_exists: Boolean
  partnerOrSponsor: Boolean
  partnerOrSponsor_not: Boolean
  partnersAndSponsorsRole_exists: Boolean
  partnersAndSponsorsRole: String
  partnersAndSponsorsRole_not: String
  partnersAndSponsorsRole_in: [String]
  partnersAndSponsorsRole_not_in: [String]
  partnersAndSponsorsRole_contains: String
  partnersAndSponsorsRole_not_contains: String
  memberLocation_exists: Boolean
  memberLocation: String
  memberLocation_not: String
  memberLocation_in: [String]
  memberLocation_not_in: [String]
  memberLocation_contains: String
  memberLocation_not_contains: String
  memberRole_exists: Boolean
  memberRole_contains_all: [String]
  memberRole_contains_some: [String]
  memberRole_contains_none: [String]
  OR: [OrganizationMemberFilter]
  AND: [OrganizationMemberFilter]
}

type OrganizationMemberLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
  blogPostCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, order: [OrganizationMemberLinkingCollectionsBlogPostCollectionOrder]): BlogPostCollection
  updatesAlertCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, order: [OrganizationMemberLinkingCollectionsUpdatesAlertCollectionOrder]): UpdatesAlertCollection
}

enum OrganizationMemberLinkingCollectionsBlogPostCollectionOrder {
  postType_ASC
  postType_DESC
  isPublicPost_ASC
  isPublicPost_DESC
  title_ASC
  title_DESC
  description_ASC
  description_DESC
  dateTime_ASC
  dateTime_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

enum OrganizationMemberLinkingCollectionsUpdatesAlertCollectionOrder {
  title_ASC
  title_DESC
  alertEnabled_ASC
  alertEnabled_DESC
  nonContentRelatedAlert_ASC
  nonContentRelatedAlert_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

enum OrganizationMemberOrder {
  name_ASC
  name_DESC
  memberOfExecutiveBoard_ASC
  memberOfExecutiveBoard_DESC
  executiveBoardRole_ASC
  executiveBoardRole_DESC
  instructorOrIntern_ASC
  instructorOrIntern_DESC
  instructorRole_ASC
  instructorRole_DESC
  teamMember_ASC
  teamMember_DESC
  teamRole_ASC
  teamRole_DESC
  partnerOrSponsor_ASC
  partnerOrSponsor_DESC
  partnersAndSponsorsRole_ASC
  partnersAndSponsorsRole_DESC
  memberLocation_ASC
  memberLocation_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

type OrganizationMemberPostsCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [BlogPost]!
}

enum OrganizationMemberPostsCollectionOrder {
  postType_ASC
  postType_DESC
  isPublicPost_ASC
  isPublicPost_DESC
  title_ASC
  title_DESC
  description_ASC
  description_DESC
  dateTime_ASC
  dateTime_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

"""
The 'Quality' type represents quality as whole numeric values between `1` and `100`.
"""
scalar Quality

type Query {
  asset(id: String!, preview: Boolean, locale: String): Asset
  assetCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: AssetFilter, order: [AssetOrder]): AssetCollection
  blogPost(id: String!, preview: Boolean, locale: String): BlogPost
  blogPostCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: BlogPostFilter, order: [BlogPostOrder]): BlogPostCollection
  organizationMember(id: String!, preview: Boolean, locale: String): OrganizationMember
  organizationMemberCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: OrganizationMemberFilter, order: [OrganizationMemberOrder]): OrganizationMemberCollection
  updatesAlert(id: String!, preview: Boolean, locale: String): UpdatesAlert
  updatesAlertCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: UpdatesAlertFilter, order: [UpdatesAlertOrder]): UpdatesAlertCollection
  facesOfGoe(id: String!, preview: Boolean, locale: String): FacesOfGoe
  facesOfGoeCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: FacesOfGoeFilter, order: [FacesOfGoeOrder]): FacesOfGoeCollection
  aboutPageContent(id: String!, preview: Boolean, locale: String): AboutPageContent
  aboutPageContentCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: AboutPageContentFilter, order: [AboutPageContentOrder]): AboutPageContentCollection
  homePageContent(id: String!, preview: Boolean, locale: String): HomePageContent
  homePageContentCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: HomePageContentFilter, order: [HomePageContentOrder]): HomePageContentCollection
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String, where: EntryFilter, order: [EntryOrder]): EntryCollection
  _node(id: ID!, preview: Boolean, locale: String): _Node
}

interface ResourceLink {
  sys: ResourceSys!
}

type ResourceSys {
  urn: String!
  linkType: String!
}

type Sys {
  id: String!
  spaceId: String!
  environmentId: String!
  publishedAt: DateTime
  firstPublishedAt: DateTime
  publishedVersion: Int
}

input SysFilter {
  id_exists: Boolean
  id: String
  id_not: String
  id_in: [String]
  id_not_in: [String]
  id_contains: String
  id_not_contains: String
  publishedAt_exists: Boolean
  publishedAt: DateTime
  publishedAt_not: DateTime
  publishedAt_in: [DateTime]
  publishedAt_not_in: [DateTime]
  publishedAt_gt: DateTime
  publishedAt_gte: DateTime
  publishedAt_lt: DateTime
  publishedAt_lte: DateTime
  firstPublishedAt_exists: Boolean
  firstPublishedAt: DateTime
  firstPublishedAt_not: DateTime
  firstPublishedAt_in: [DateTime]
  firstPublishedAt_not_in: [DateTime]
  firstPublishedAt_gt: DateTime
  firstPublishedAt_gte: DateTime
  firstPublishedAt_lt: DateTime
  firstPublishedAt_lte: DateTime
  publishedVersion_exists: Boolean
  publishedVersion: Float
  publishedVersion_not: Float
  publishedVersion_in: [Float]
  publishedVersion_not_in: [Float]
  publishedVersion_gt: Float
  publishedVersion_gte: Float
  publishedVersion_lt: Float
  publishedVersion_lte: Float
}

"""
Use this field to create an alert for the home page when new content/updates are
available.  [See type definition](https://app.contentful.com/spaces/9bvr8ot1gwtw/content_types/updatesAlert)
"""
type UpdatesAlert implements Entry {
  sys: Sys!
  contentfulMetadata: ContentfulMetadata!
  linkedFrom(allowedLocales: [String]): UpdatesAlertLinkingCollections
  title(locale: String): String
  alertEnabled(locale: String): Boolean
  contentReference(preview: Boolean, locale: String): UpdatesAlertContentReference
  nonContentRelatedAlert(locale: String): String
}

type UpdatesAlertCollection {
  total: Int!
  skip: Int!
  limit: Int!
  items: [UpdatesAlert]!
}

union UpdatesAlertContentReference = BlogPost | OrganizationMember

input UpdatesAlertFilter {
  sys: SysFilter
  contentfulMetadata: ContentfulMetadataFilter
  title_exists: Boolean
  title: String
  title_not: String
  title_in: [String]
  title_not_in: [String]
  title_contains: String
  title_not_contains: String
  alertEnabled_exists: Boolean
  alertEnabled: Boolean
  alertEnabled_not: Boolean
  contentReference_exists: Boolean
  nonContentRelatedAlert_exists: Boolean
  nonContentRelatedAlert: String
  nonContentRelatedAlert_not: String
  nonContentRelatedAlert_in: [String]
  nonContentRelatedAlert_not_in: [String]
  nonContentRelatedAlert_contains: String
  nonContentRelatedAlert_not_contains: String
  OR: [UpdatesAlertFilter]
  AND: [UpdatesAlertFilter]
}

type UpdatesAlertLinkingCollections {
  entryCollection(skip: Int = 0, limit: Int = 100, preview: Boolean, locale: String): EntryCollection
}

enum UpdatesAlertOrder {
  title_ASC
  title_DESC
  alertEnabled_ASC
  alertEnabled_DESC
  nonContentRelatedAlert_ASC
  nonContentRelatedAlert_DESC
  sys_id_ASC
  sys_id_DESC
  sys_publishedAt_ASC
  sys_publishedAt_DESC
  sys_firstPublishedAt_ASC
  sys_firstPublishedAt_DESC
  sys_publishedVersion_ASC
  sys_publishedVersion_DESC
}

