export default function RenderMember({ activeDepartment, member }) {
  // Function to render the role based on activeDepartment and member's roles
  const renderRole = () => {
    switch (activeDepartment) {
      case "Team":
        return member.teamRole ? <p>{member.teamRole}</p> : null;
      case "Advisory Board":
        return member.affiliateRole ? <p>{member.affiliateRole}</p> : null;
      case "Partners & Sponsors":
        return member.affiliateRole ? <p>{member.affiliateRole}</p> : null;
      case "Instructors & Assistants":
        return (
          <>
            {member.instructorRole && <p>{member.instructorRole}</p>}
            {member.assistantRole && <p>{member.assistantRole}</p>}
          </>
        );
      default:
        return null;
    }
  };

  return (
    <div className="rounded-2xl mb-11 bg-primary">
      <img
        src={member.profilePicture.url}
        alt={`Image of ${member.name}`}
        className="h-[384px] w-[384px] object-cover rounded-t-md"
      />
      <div className="bg-primary rounded-b-md p-4 text-center">
        <h1 className="font-oswald text-lg">{member.name}</h1>
        {renderRole()}
      </div>
    </div>
  );
}
