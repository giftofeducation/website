export default function RenderDonationMethods({ methods }) {
  return methods.map((method, index) => {
    return method.donationUrl ? (
      <a
        href={method.donationUrl}
        rel="noopener noreferrer"
        className={`w-[300px] h-[300px] inline-flex relative transform cursor-pointer overflow-hidden rounded-3xl transition-transform duration-300 hover:-translate-y-1 hover:shadow-lg ${index === 0 ? "ml-0 md:mb-0 mb-12" : "md:ml-24 ml-0 md:mb-0 mb-12"}`}
        key={method.sys.id}
      >
        <img src={method.donationImage.url} />
      </a>
    ) : (
      <img
        src={method.donationImage.url}
        className={`w-[300px] h-[300px] ${index === 0 ? "ml-0 md:mb-0 mb-12" : "md:ml-24 ml-0 md:mb-0 mb-12"} inline-flex`}
        key={method.sys.id}
      />
    );
  });
}
