export default function PostsByYear({ handleYearClick, posts, selectedYear }) {
  return (
    <div className="flex flex-col pt-8 mb-8 md:mr-24">
      <h3 className="mb-2 font-openSans font-semibold">Filter by year:</h3>
      {/* reverse the posts order so they display most recent year descending */}
      {[...posts].reverse().map(({ year }) => {
        return (
          <div key={year}>
            <p
              className={`font-openSans mb-2 cursor-pointer rounded-3xl px-6 py-1 inline-flex ${year === selectedYear ? "bg-primary hover:bg-primary" : "bg-white hover:bg-primary/40"}`}
              onClick={() => handleYearClick(year)}
            >
              {year}
            </p>
          </div>
        );
      })}
    </div>
  );
}
