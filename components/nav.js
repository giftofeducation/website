"use client";
import Link from "next/link";
import { useState } from "react";
import { usePathname } from "next/navigation";
import routes from "@/lib/routes";
import Logo from "./logo";

const RenderRoutes = ({ mobileMenuOpen, setMobileMenuOpen, mobile }) => {
  const pathname = usePathname();
  return routes.map((route) => (
    <li
      key={route.label}
      className={`font-oswald text-base ${pathname === route.path ? "text-primary" : "text-base-100"} md:ml-12 mb-4 last:mb-0 md:mb-0 hover:text-primary active:text-primary focus:text-primary visited:text-primary cursor-pointer`}
    >
      <Link
        href={route.path}
        onClick={
          mobile === true
            ? () => setMobileMenuOpen(!mobileMenuOpen)
            : () => setMobileMenuOpen(false)
        }
      >
        {route.label}
      </Link>
    </li>
  ));
};

export default function Nav() {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

  return (
    <div className="navbar bg-neutral px-8">
      {/* Logo */}
      <div className="navbar-start">
        <Link className="flex items-center" href="/">
          <Logo />
          <p className="text-base-100 font-oswald text-base font-extralight tracking-widest leading-6 ml-4 no-underline p-0">
            Gift of Education
          </p>
        </Link>
      </div>
      {/* Menus */}
      <div className="md:ml-auto navbar-end">
        {/* Desktop Menu */}
        <ul className="flex-row hidden md:flex justify-around">
          <RenderRoutes
            {...{ mobileMenuOpen, setMobileMenuOpen }}
            mobile={false}
          />
        </ul>

        {/* Mobile Menu */}
        <div>
          <div
            tabIndex={0}
            role="button"
            className="btn btn-neutral md:hidden flex"
            onClick={() => setMobileMenuOpen(!mobileMenuOpen)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16"
              />
            </svg>
          </div>
          <ul
            tabIndex={0}
            className={`absolute top-[64px] bg-neutral rounded-b-2xl w-48 px-6 pb-6 pt-4 right-[42px] z-10  ${mobileMenuOpen === true ? "flex flex-col" : "hidden"}`}
          >
            <RenderRoutes
              {...{ mobileMenuOpen, setMobileMenuOpen }}
              mobile={true}
            />
          </ul>
        </div>
      </div>
    </div>
  );
}
