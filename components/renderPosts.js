import PostCard from "./postCard";

export default function RenderPosts({ selectedTag, selectedYear, posts }) {
  // Filter Posts by tag. If "all tags", render all posts.
  // Otherwise, render posts that include the selected tag
  const filteredPosts =
    selectedTag === "all tags"
      ? posts
      : posts.filter((post) => post.tags.includes(selectedTag));

  return (
    <div className="flex flex-col items-center text-center flex-grow py-8 h-[calc(100vh-261px)] overflow-y-auto overflow-x-hidden scrollbar-hide">
      {selectedYear &&
        filteredPosts.map((post) => {
          const postPath = `/blog/posts/${post.slug}?id=${post.sys.id}`;
          return (
            <PostCard
              {...{ post, postPath }}
              key={post.sys.id}
              displayTags={true}
            />
          );
        })}
    </div>
  );
}
