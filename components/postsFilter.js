import PostsByYear from "./postsByYear";
import TagsByYear from "./tagsByYear";

export default function PostsFilter({
  handleSelectTag,
  handleYearClick,
  posts,
  selectedTag,
  selectedYear,
  selectedYearPosts,
}) {
  // Create an array of tags to filter by
  // Add in "all tags" to reset filter.
  const tags = [
    "all tags",
    ...new Set(
      selectedYearPosts.posts.flatMap((item) =>
        item.tags.map((tag) => tag.toLowerCase()),
      ),
    ),
  ];

  return (
    <div>
      <PostsByYear {...{ handleYearClick, posts, selectedYear }} />
      <TagsByYear {...{ handleSelectTag, selectedTag, tags }} />
    </div>
  );
}
