export default function MembersNav({
  activeDepartment,
  handleSetActiveDepartment,
}) {
  const nav = [
    "Team",
    "Advisory Board",
    "Partners & Sponsors",
    "Instructors & Assistants",
  ];

  return (
    <div className="flex flex-row items-center md:w-[80%] justify-evenly mx-auto py-4 md:py-16">
      {nav.map((item) => {
        return (
          <h1
            key={item}
            className={`font-oswald text-md md:text-xl cursor-pointer mx-1 text-center md:text-left md:mx-0 ${activeDepartment === item ? "underline" : "hover:underline"}`}
            onClick={() => handleSetActiveDepartment(item)}
          >
            {item}
          </h1>
        );
      })}
    </div>
  );
}
