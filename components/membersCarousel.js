"use client";

import { Splide, SplideSlide } from "@splidejs/react-splide";
import RenderMember from "./renderMember";
import "../app/splide.min.css";

const MembersCarousel = ({ activeDepartment, members }) => {
  return (
    <div className="block sm:hidden mb-8">
      {/* Visible only on mobile (below sm: 640px) */}
      <Splide
        options={{
          rewind: true, // Loops back to start
          perPage: 1, // One member per view
          perMove: 1,
          gap: "1rem",
          arrows: false,
          pagination: true, // Optional dots
          drag: true, // Enables swipe on mobile
          classes: {
            pagination: "splide__pagination",
          },
        }}
      >
        {members.map((member) => (
          <SplideSlide key={member.sys.id}>
            <div className="flex flex-col items-center p-4">
              <RenderMember {...{ activeDepartment, member }} />
            </div>
          </SplideSlide>
        ))}
      </Splide>
    </div>
  );
};

export default MembersCarousel;
