import RenderDonationMethods from "./renderDonationMethods";

export default function DonationBanner({ banner }) {
  return (
    <div className="hero bg-primary/45 py-12 md:py-18">
      <div className="hero-content text-center text-[#222222] py-20 flex flex-col">
        <div className="max-w-[85%] md:max-w-4xl">
          <h1 className="mb-10 text-2xl md:text-4xl font-normal font-oswald">
            {banner.headingText}
          </h1>
          <p className="mb-10 md:text-lg font-openSans whitespace-pre-wrap">
            {banner.textField}
          </p>
        </div>
        <div className="flex flex-col md:flex-row mt-0 md:mt-8">
          <RenderDonationMethods methods={banner.qrCodes} />
        </div>
      </div>
    </div>
  );
}
