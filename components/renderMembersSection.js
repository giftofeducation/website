"use client";

import { useState } from "react";
import MembersNav from "./membersNav";
import RenderMemberSection from "./renderMemberSection";

export default function RenderMembersSection({ members }) {
  const [activeDepartment, setActiveDepartment] = useState("Team");

  const handleSetActiveDepartment = (item) => {
    setActiveDepartment(item);
  };

  return (
    <div>
      <MembersNav {...{ activeDepartment, handleSetActiveDepartment }} />
      <RenderMemberSection {...{ activeDepartment, members }} />
    </div>
  );
}
