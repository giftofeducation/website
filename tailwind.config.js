/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        openSans: ["var(--font-open-sans)"],
        oswald: ["var(--font-oswald)"],
      },
      colors: {
        goePink: "#AD356D",
        goeGold: "#FCBD55",
        goeGrey: "#222222",
      },
    },
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: "#fcbd55",
          secondary: "#ad356d",
          accent: "#ece",
          neutral: "#110c09",
          "base-100": "#ffffff",
          info: "#ad356d",
          success: "#1ce3af",
          warning: "#f2ff00",
          error: "#f43f5e",
          "off-black": "#222222",
        },
      },
    ],
  },
};
