import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import { membersQuery } from "@/lib/queries/membersQuery";
import RenderMembersSection from "@/components/renderMembersSection";

export default async function Members() {
  const query = membersQuery;

  const {
    groupedMembersByRoleCollection: { items },
  } = await makeGraphQLRequest({ query });

  return <RenderMembersSection members={items} />;
}
