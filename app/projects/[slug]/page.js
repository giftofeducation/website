import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import { projectPostQuery } from "@/lib/queries/projectsQuery";
import RenderPost from "@/components/renderPost";

// Use nextjs app router searchParams to access post ID
export default async function Project({ searchParams }) {
  const query = projectPostQuery;
  const variables = {
    id: searchParams.id,
  };
  const { blogPost } = await makeGraphQLRequest({ query, variables });

  return (
    <div>
      <RenderPost {...{ blogPost }} />
    </div>
  );
}
