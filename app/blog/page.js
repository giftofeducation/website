import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import { blogPostsQuery } from "@/lib/queries/blogQuery";
import PostsSection from "@/components/postsSection";

export default async function Blog() {
  const query = blogPostsQuery;

  const {
    blogPostCollection: { items },
  } = await makeGraphQLRequest({ query });

  // Group Posts by Year
  const groupedPosts = items.reduce((acc, item) => {
    const dateTime = item.dateTime || item.sys.firstPublishedAt; // Use dateTime if available, otherwise use firstPublishedAt
    const year = new Date(dateTime).getFullYear().toString();
    if (!acc[year]) {
      acc[year] = [];
    }
    acc[year].push(item);
    return acc;
  }, {});

  // Transform grouped posts into desired output format
  const posts = Object.keys(groupedPosts).map((year) => ({
    year,
    posts: groupedPosts[year],
  }));

  return (
    <div>
      <div className="w-[80%] mx-auto text-center py-12">
        <h1 className="font-oswald text-4xl md:text-5xl font-normal">Blog</h1>
      </div>
      <PostsSection {...{ posts }} />
    </div>
  );
}
