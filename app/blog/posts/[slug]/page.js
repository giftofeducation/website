import ReactMarkdown from "react-markdown";
import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import { blogPostQuery } from "@/lib/queries/blogQuery";
import formatDate from "@/lib/utils/formatDate";
import RenderPost from "@/components/renderPost";

const RenderPostDetails = ({ author, banner, dateTime, tags }) => {
  return (
    <>
      <div className="flex flex-row items-center justify-center mb-4">
        <p className={`mr-8 ${banner && "text-lg"}`}>By: {author.name}</p>
        <p className={`${banner && "text-lg"}`}>{formatDate(dateTime)}</p>
      </div>
      <ul className="flex items-center justify-center mb-6">
        {tags.map((tag) => (
          <li
            key={tag}
            className={`badge badge-primary mr-4 ${banner && "text-lg px-4 py-3"}`}
          >
            {tag}
          </li>
        ))}
      </ul>
    </>
  );
};

// Use nextjs app router searchParams to access post ID
export default async function Post({ searchParams }) {
  const query = blogPostQuery;
  const variables = {
    id: searchParams.id,
  };
  const { blogPost } = await makeGraphQLRequest({ query, variables });

  return (
    <div>
      {/* Render two different UIs based on the presence of a postBannerImage */}
      <RenderPost {...{ blogPost }} />
    </div>
  );
}
