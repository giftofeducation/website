import ReactMarkdown from "react-markdown";
import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import { donationQuery } from "@/lib/queries/donationQuery";
// import RenderDonationMethods from "@/components/renderDonationMethods";
import DonationBanner from "@/components/donationBanner";

export default async function Donate() {
  const query = donationQuery;
  const {
    donationPageContent: {
      bannerImage,
      donationAppeal,
      donationMethodsCollection,
      donationMethodsDescription,
      donationMethodsSectionHeading,
      pageTitle,
    },
  } = await makeGraphQLRequest({ query });

  const banner = {
    headingText: donationMethodsSectionHeading,
    textField: donationMethodsDescription,
    qrCodes: donationMethodsCollection.items,
  };

  return (
    <>
      <div
        className="hero min-h-[50vh] md:min-h-screen"
        style={{
          backgroundImage: `url(${bannerImage.url})`,
        }}
      >
        <div className="hero-overlay bg-opacity-60"></div>
        <div className="hero-content text-center text-base-100">
          <div className="max-w-4xl">
            <h1 className="mb-10 text-3xl md:text-5xl font-normal font-oswald">
              {pageTitle}
            </h1>
            <ReactMarkdown className="mb-10 text-lg font-openSans prose text-white max-w-full">
              {donationAppeal}
            </ReactMarkdown>
          </div>
        </div>
      </div>
      <DonationBanner {...{ banner }} />
    </>
  );
}
