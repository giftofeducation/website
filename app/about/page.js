import ReactMarkdown from "react-markdown";
import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import aboutQuery from "@/lib/queries/aboutQuery";

// TODO - contentful
// Create Hero Image entry for about page
export default async function About() {
  const query = aboutQuery;
  const {
    aboutPageContent: { aboutUs, heroHeading },
  } = await makeGraphQLRequest({ query });

  return (
    <>
      <div
        className="hero h-[450px] md:h-[650px]"
        style={{
          backgroundImage: `url(http://images.ctfassets.net/9bvr8ot1gwtw/3SVr9qqp9GzJIPCgGNOIWg/e27544f42380efcbe73fccc72f81d18b/engagedteachers.jpg)`,
        }}
      >
        <div className="hero-overlay bg-opacity-50"></div>
        <div className="hero-content text-center text-base-100">
          <div className="max-w-3xl">
            <h1 className="mb-10 text-3xl md:text-5xl font-normal font-oswald">
              {heroHeading}
            </h1>
          </div>
        </div>
      </div>
      <div className="prose md:prose-lg max-w-[80%] p-8 md:p-24 mx-auto">
        <ReactMarkdown>{aboutUs}</ReactMarkdown>
      </div>
    </>
  );
}
