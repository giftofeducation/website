import { makeGraphQLRequest } from "@/lib/makeGraphQLRequest";
import homeQuery from "@/lib/queries/homeQuery";

import Hero from "@/components/hero";
import MissionVision from "@/components/missionVision";
import DonationBanner from "@/components/donationBanner";

export default async function Home() {
  const query = homeQuery;
  const { homePageContent } = await makeGraphQLRequest({ query });

  const {
    heroImage,
    heroTagline,
    missionStatement,
    visionStatement,
    bannerHeadingText,
    bannerTextField,
    qrCodesCollection,
  } = homePageContent;

  const banner = {
    headingText: bannerHeadingText,
    textField: bannerTextField,
    qrCodes: qrCodesCollection.items,
  };

  return (
    <main>
      <Hero {...{ heroImage, heroTagline }} />
      <MissionVision {...{ missionStatement, visionStatement }} />
      <DonationBanner {...{ banner }} />
    </main>
  );
}
